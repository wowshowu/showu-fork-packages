

## Importing package

```
lerna import {LOCAL_PROJECT_PATH} --flatten
```

## publish
```
lerna publish -y --no-verify-access patch
```
